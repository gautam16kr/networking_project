import socket
import sys
from numpy import random

fileName = sys.argv[1]
n = int(sys.argv[2])

def randomIP():
	parts = random.randint(0,255,4)
	parts = map(lambda x: str(x) , parts)
	return ".".join(parts)

for i in range(n):
	print(randomIP())