import socket
import sys

file = open( sys.argv[1],"r" )
file2 = open( sys.argv[1] + ".fixed" ,"w" )

for line in file:
	a = line[:-1].split(' ')
	CIDR = a[0].split('/')
	interface = a[1]

	subnet = '1'* int (CIDR[1])
	subnet += '0' * (32 - len(subnet))

	ip_parts = CIDR[0].split('.')

	subnet_parts = [subnet[0:8],subnet[8:16], subnet[16:24], subnet[24:32] ]
	
	last = []
	for i in range(4):
		x = int(ip_parts[i]) 
		y = int(subnet_parts[i], 2)
		last.append( x & y )

	for i in range(4):
		last[i] = str(last[i])
	
	file2.write(".".join(last) + "/" + CIDR[1] + " " + interface + "\n" )

file.close()
file2.close()