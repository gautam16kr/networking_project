/*
............................................................................................................
  
    F A S T   F O R W A R D I N G   T A B L E I M P L E M E N T A T I O N
    U S I N G  T R I E

............................................................................................................

    P A R T H    P A T E L     |  1 1 1 5 0 1 0 1 9
    A R Y A N    R A J         |  1 1 1 5 0 1 0 0 7
    G A U T A M  K U M A R     |  1 1 1 5 0 1 0 0 8

............................................................................................................

*/


#include <bits/stdc++.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_link.h>

using namespace std;

////////////////////////////////////////////// MACROS /////////////////////////////////////////////////////
 
#define MAX_INTERFACE_NAME_LEN 50
#define MAX_N_INTERFACES 10
//IF TRUE, one can insert only system's available interfaces (INET)
#define CHECK_INTERFACE false

/////////////////////////////////////////////// GLOBAL VARIABLES //////////////////////////////////////////

// stores systems's network interfaces ( only IF_INET )
char *sys_interfaces[MAX_N_INTERFACES];
// DEFAULT OUTPUT INTERFACE
// If there is no entry matching for asked IP, returns this interface
char default_interface[MAX_INTERFACE_NAME_LEN]= "default" ;
// pointer to the last inserted system interface
unsigned short curIntefacePtr = 0;

<<<<<<< HEAD
//////////////////////////////////////////////// FUNCTIONS ////////////////////////////////////////////////

// True if IP is valid, else false
=======

>>>>>>> b1ef31aa9b4642e3b27fc89d6335d08ed77e0759
bool is_valid_ip(char *ip)
{
    if (!ip)
        return false;
    
    int i, num, dots = 0;
    char ip_str[33];
    char *ptr;
    strcpy(ip_str,ip);
    //ip_str="12.34.45.56"
   
    ptr = strtok(ip_str, ".");
 
    if (ptr == NULL)
        return false;
 
    while (ptr) {
        num = atoi(ptr);       
        if (num >= 0 && num <= 255) {
            ptr = strtok(NULL,".");
            if (ptr) {
                ++dots; if ( dots > 3) return false;
            }
        } else return false;
    }
    // if ip contains more than more than 3 '.'
    if (dots != 3) return false;
    return true;
}

// TrieNode structure defn
struct TrieNode
{
    struct TrieNode *children[2];
    char interface [MAX_INTERFACE_NAME_LEN];
    bool end_of_network_ip;
};

// CHECKS only if MACRO `CHECK_INTERFACE` = True
// returns TRUE if interafce is available in CURRENT System
bool is_interface_available ( char interface[MAX_INTERFACE_NAME_LEN]){
    for ( int i = 0 ;  i < curIntefacePtr ; i ++ )
        if ( strcmp (sys_interfaces[i], interface) == 0 )
            return true;
    return ! CHECK_INTERFACE ;
}

// refresh the available interfaces
// stores into GLOBAL variable : sys_interfaces
bool update_interfaces (){

    struct ifaddrs *ifaddr, *ifa;
    int family, s, n;
    char host[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1) {
        perror("getifaddrs"); return false;
    }

    curIntefacePtr = 0; // reset

    for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
        if (ifa->ifa_addr == NULL)
            continue;

        if ( ifa->ifa_addr->sa_family == AF_INET )
            sys_interfaces[curIntefacePtr++] = ifa->ifa_name;
    }

    freeifaddrs(ifaddr); return true;
}


// Returns fresh trie node
struct TrieNode *getNode(void)
{
    struct TrieNode *pNode =  new TrieNode;
    // if node creation failed
    if(pNode == NULL) return NULL;
 
    pNode->end_of_network_ip = false;
    pNode->children[0] = NULL;
    pNode->children[1] = NULL;
 
    return pNode;
}
 

// insert entry into TRIE dataStructure rooted at `root`
// returns True on succes else False
bool insert(struct TrieNode *root, char ip_str[16] , unsigned short int subnet, char interface[MAX_INTERFACE_NAME_LEN])
{
    // ipv4 : str -> bitpacking
    struct in_addr ip;
    inet_aton(ip_str, &ip);
    ip.s_addr = htonl(ip.s_addr);

    struct TrieNode *pCrawl = root;
 
    for (unsigned long int i = 31 ; i > 31 - subnet ; i--)
    {
        unsigned long int index = (ip.s_addr >> i) & 1 ;

        if (!pCrawl->children[index])
            pCrawl->children[index] = getNode();
            //if node creation failed
            if ( pCrawl->children[index]  == NULL )
                return false;

        pCrawl = pCrawl->children[index];
    }
    // MARK AS THE END OF NETWORK IP 
    pCrawl->end_of_network_ip = true;
    // STORE CORRESPONDING INTERFACE NAME
    strcpy(pCrawl->interface , interface);
}
 
// If Matchin Entry Found, prints OUTPUT INTERFACE & returns True
// else returns False
bool search(struct TrieNode *root, char ip_str[16])
{   
    // ipv4 : str -> bitpacking
    struct in_addr ip;
    inet_aton(ip_str, &ip);
    ip.s_addr = htonl(ip.s_addr);
    // initisation
    bool flag = false; // true if any match was found
    char last_matched_interface[MAX_INTERFACE_NAME_LEN] = ""; // last matched interface during crawling
    // root (forwardingTable)
    struct TrieNode *pCrawl = root;

    // crawling TRIE datastructure
    for (unsigned long int i = 31; i >= 0 ; i--)
    {
        unsigned long int index = (ip.s_addr >> i) & 1 ;
        // if any networkID is matched, store destination interfaceName
        if ( pCrawl->end_of_network_ip ) {
            flag = true;
            strcpy(last_matched_interface, pCrawl->interface);
        }
        // if nothing to crawl
        if (!pCrawl->children[index]) {
            // if last matched interfaceName
            if ( flag ) cout << last_matched_interface << "\n" ;
            return flag ;
        
        } else pCrawl = pCrawl->children[index];
    }
}

// dump, passed arguments to file pointed by fp
// Dump Format : IP/NETWORKMASK INTERFACE
bool dump_entry( FILE* fp, char ip_str[33], char interface[MAX_INTERFACE_NAME_LEN]) {
    
    if(!fp){
        cout << "Invalid File Pointer!" << endl;
        return false;
    }
    // dumping : ip/subnet interfaceName
      
        // concatenet 0s to make ip length 32
        unsigned short int len = strlen(ip_str);
        if ( len < 32 ) for ( unsigned short int i = len ; i < 32 ; i++ ) ip_str[i] = '0';
        ip_str[33] = '\0';
        
        // 4 parts of ip is stored in it
        unsigned short int ip4_parts[4] ;
        unsigned short int ip4_part ;

        for ( int i = 0; i < 4 ; i++ ) {
            ip4_part = 0;
            for ( int j = 0 ; j < 8 ; j++ ){
                if ( ip_str[8*i + j] == '1' ){
                    ip4_part += (unsigned short int) 1 << 7-j;
                }
            }
            ip4_parts[i] = ip4_part;
        }

        for ( int i = 0 ; i < 3 ; i++ )
            fprintf(fp, "%d.", ip4_parts[i]);
        fprintf(fp, "%d/%u %s\n", ip4_parts[3], len, interface);

    return true;
}

// Traverse Trie rooted at `root`
// If node is end of some network ip, then dump entry to file `fp`
// Dump Format : NETWORK_IP(with appended 0s)/NetworkMask INTERFACE
bool traverse(struct TrieNode *root, char network_ip[33] , FILE *fp) {
    if(!root || !fp) {
        cout << "Invalid ROOT or File Pointer." << endl; return false;
    }
    // dump entry to file `fp`
    if ( root->end_of_network_ip ) {
        char ip_tmp[33] ;
        strcpy(ip_tmp, network_ip);
        dump_entry( fp, ip_tmp, root->interface );
    }

    unsigned short int len = strlen(network_ip);
    
    // If left child exists
    if ( root->children[0] ) {
        network_ip[ len ] = '0';
        network_ip[ len + 1 ] = '\0';
        traverse( root->children[0], network_ip, fp );
    } 
    // If right child exists
    if ( root->children[1] ) {
        network_ip[ len ] = '1';
        network_ip[ len + 1 ] = '\0';
        traverse( root->children[1], network_ip, fp );
    }

    return true;
}

// Dump Forwarding Table to `fileName`
bool dumpForwardingTable ( struct TrieNode *root, char fileName[255] ) {

    FILE * fp = fopen(fileName, "w");   
    char ip_fraction[33] = "";
    traverse( root, ip_fraction, fp);
    fclose(fp);
    cout << "Succesfull : Forwarding Table -> " << fileName << endl;
    return true;
}
void importfile(char *filename,struct TrieNode *root){
    FILE *fp = NULL ;
    char command[100];
    char CIDR[100];
    char interface[MAX_INTERFACE_NAME_LEN];
    char *ip, *mask;
    int num ;
    unsigned long int lineNum = 0; 

    fp = fopen ( filename, "r" ) ;
    if(!fp){
        printf("file couldnt be opened\n");
        return;
    }
// loop through file, add valid entries into Forwarding Table
// Prints Errors
    
    while(!feof(fp)){
    
        lineNum++;
        if(fscanf(fp,"%s",CIDR) == -1) break;
        fscanf(fp,"%s",interface);
            
        ip=strtok(CIDR, "/");
        mask=strtok(NULL, "/");
        
        if(!(is_valid_ip(ip))) printf("found INVALID IP in line %d\n",lineNum);
        
        num = atoi(mask);
        
        if(num >32 || num<1) printf("found INVALID mask in line %d\n",lineNum);
        else if ( !is_interface_available(interface) ) printf("found INVALID interface in line %d\n",lineNum);
        else insert( root ,ip , num, interface );
    }

} 



//////////////////////////////////////// MAIN ////////////////////////////////////////////////////



int main(int argc, char const *argv[])
{
    // refreshes available interfaces
    if (!update_interfaces()) {
        cout << "Couldn't fetch System's avaialble Interfaces." << endl;
        return -1;
    }
    
    // create fersh FORWARDING TABLE 
    struct TrieNode *root = getNode();

    FILE *fp = NULL ;
    char command[100], filename[100];
    char CIDR[100];
    char interface[MAX_INTERFACE_NAME_LEN];
    char *ip, *mask;
    int num ;
    unsigned long int lineNum = 0; // to print the ERROR LINE NUMBER

    // if invalid comandline arguments are passed
    if ( ( argc != 1 && argc != 3 ) || ( argc == 3 && strcmp (argv[1] , "-f" ) != 0 )) {
        cout << "Invalid Commandline Arguments." << endl;
        cout << "Options : -f fileNAme" << endl;
        return -1;
    }

    // if -f fileName is given
    if( argc == 3 ) {   
        strcpy( filename , argv[2] ) ;
        importfile(filename,root);
    }
    
    char argument[100];

    

    //////////////////////////////////////// 
    ////        OUR TERMINAL    
    //////////////////////////////////////// 
    
    while(1)
    {
        printf(">>");
        scanf("%s",command);



        // EXIT
        if ( strcmp(command,"exit") == 0 ) break;
        
        
        // DUMP FORWARDING TABLE
        if ( strcmp (command,"dump") == 0 ) {
            scanf("%s",argument);
            dumpForwardingTable( root, argument ); continue;
        }

        // Search OUTPUT Interface

        if( strcmp( command, "search" ) == 0 ) {
            
            scanf("%s",argument);

            if( !is_valid_ip(argument) ) {
                printf("INVALID IP.\n"); continue; 
            }
            
            if(!search (root, argument)) cout<< default_interface << endl ; 
            continue ;
        }
        if( strcmp( command, "import" ) == 0 ){
            scanf("%s",filename);
            importfile(filename,root);
            continue;
        }
        if( strcmp( command, "changeDefault" ) == 0 ){
            scanf("%s",argument);
            strcpy(default_interface,argument);           
            continue;
        }

        scanf("%s",argument);
        if(strcmp(command,"insert")==0) {
            ip = strtok(argument, "/");
            mask=strtok(NULL, "/");
            if(mask==NULL){
                printf("invalid CIDR\n");
                continue;
            }
            num=atoi(mask);
            
            if( !is_valid_ip(ip) ) {
                printf("INVALID IP.\n"); continue;
            
            } else if( num > 32 || num < 1) {
                printf("INVALID MASK\n"); continue;
            }

            
            // INSERT
            scanf("%s",interface);

            if( !is_interface_available(interface) ) {
                printf("INVALID INTERFACE\n"); continue;
            }


            insert( root, ip, num, interface ); continue ;
        }

        // if nothing matches
        printf("unrecognized command\n");
    }
    return 0;
}