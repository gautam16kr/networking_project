# FORWARDING TABLES IMPLEMENTATION USING TRIE

---

## Introduction

This is an implementation of Forwarding Tables with use of TRIE data structure. User can use the command line interface or run the program directly.

---

## Getting Started

1. Download this repository.
2. Open the terminal and go to the downloaded folder
3. Compile **forwardingTable.cpp** with **g++**:  `g++ forwardingTable.cpp -o a`
4. Run the compiled file by `./a [-f filename]`
5. CLI commands available: `import FILENAME`, `insert IP/mask interface`, `search IP`, `dump FILENAME`,  `exit`, `help`, `changeDefault new_default_interface`

---

## Brief Description

Trie is an efficient information retrival data structure. Insert and search costs O(key_length), however the memory requirements of Trie is O(ALPHABET_SIZE * key_length * N) where N is number of keys in Trie.

#### CLI description
* `import FILENAME`: import the file into forwarding table. Alternate is to give -f flag in command line
* `insert IP/mask interface`: Insert a new IP range to forwarding table
* `search IP`: Search the output interface for the IP
* `dump FILENAME`: Dump the forwarding table into the file
* `exit`: Exit from CLI
* `help` : show all commands
* `changeDefault new_default_interface` : change default OUTPUT Interface
---

## Contributors

1. [Aryan Raj](https://bitbucket.org/aryan_raj/)
2. [Parth Patel](https://bitbucket.org/PArth__PAtel/)
3. [Gautam Kumar](https://bitbucket.org/gautam16kr/)

---

## License

This project is licensed under GNU GPLv3. Find the copy of LICENSE [here](https://bitbucket.org/gautam16kr/networking_project/src/196227389c1c972e39bf46691577bd85b5fa53ab/LICENSE?at=master).
